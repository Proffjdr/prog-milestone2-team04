﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Menu
{
    class Program
    {

        static void Main(string[] args)
        {

        Start:
            Console.Clear();

            Console.WriteLine("+=-=-=-=-=-=-=-=+");
            Console.WriteLine("|               |");
            Console.WriteLine("|   Team 04     |");
            Console.WriteLine("|               |");
            Console.WriteLine("+=-=-=-=-=-=-=-=+");
            Console.WriteLine("1. Task 1");
            Console.WriteLine("2. Task 2");
            Console.WriteLine("3. Task 3");
            Console.WriteLine("4. Task 4");
            Console.WriteLine("5. Quit");
            Console.WriteLine("+=-=-=-=-=-=-=-=+");

            var menu = Console.ReadLine();
            switch (menu)
            {
                case "1":
                    Task1();
                    goto Start;

                case "2":
                    Console.Clear();
                    Task2method1();
                    goto Start;

                case "3":
                    List<int> scores = new List<int>();
                    Task3game(scores);
                    goto Start;

                case "4":
                    TaskFourFillDictionary();
                    goto Start;

                case "5":
                    Console.Clear();
                    Console.WriteLine("Have a nice day!");
                    Environment.Exit(1);
                    break;

                default:
                    Console.Clear();
                    Console.WriteLine("Please Try again");
                    goto Start;
            }
        }
        static void Task1()
        {
            var date = DateTime.Now;
            Console.Clear();
        Start:
            Console.WriteLine("+=-=-=-=-=-=-=-=-=-=-=-=-=+");
            Console.WriteLine($"Today's date is {date.Day}/{date.Month}/{date.Year}");
            Console.WriteLine("+=-=-=-=-=-=-=-=-=-=-=-=-=+");
            Console.WriteLine("1. How Many days since your birthday?");
            Console.WriteLine("2. How Many years since your birthday?");
            var menu = Console.ReadLine();
            switch (menu)
            {
                case "1":
                    Console.Clear();
                    Task1bday(date);
                    break;

                case "2":
                    Console.Clear();
                    Task1byear(date);
                    break;

                default:
                    Console.Clear();
                    Console.WriteLine("Please Try again.");
                    goto Start;
            }



        }
        static void Task1bday(DateTime date)
        {
            var bday = new DateTime();
            var dayage = new TimeSpan();
        Start:
            Console.WriteLine("When is your birthday? (dd/mm/yyyy)");
            var bdaycheck = DateTime.TryParse(Console.ReadLine(), out bday);
            if (bdaycheck == true)
            {
                Console.WriteLine($"Your birthday is {bday.Day}/{bday.Month}/{bday.Year}");
                dayage = date.Date - bday.Date;
                Console.WriteLine($"Your are {dayage.Days} days old.");
                Console.WriteLine("Press Any Key to Continue");
                Console.ReadKey();

            }
            else
            {
                Console.Clear();
                Console.WriteLine("Please input a valid date");

                goto Start;
            }
        }

        static void Task1byear(DateTime date)
        {
            {
                var bday = new DateTime();

            Start:
                Console.WriteLine("When is your birthday? (dd/mm/yyyy)");
                var bdaycheck = DateTime.TryParse(Console.ReadLine(), out bday);
                if (bdaycheck == true)
                {
                    Console.WriteLine($"Your birthday is {bday.Day}/{bday.Month}/{bday.Year}");
                    Console.WriteLine($"Your are {date.Year - bday.Year} years old.");
                    Console.WriteLine();
                    Console.WriteLine("Press Any Key to Continue");
                    Console.ReadKey();
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Please input a valid date");

                    goto Start;
                }
            }
        }

        //TASK 2 STARTS HERE

        //Task2method1 is the summary of information for Task 2 of the project
        //Task2method1 stores the variables that are used amongst the other methods
        static void Task2method1()
        {
            //Storage of variables for Task2method1
            string input;
            int output = 0;
            int papers;
            int studentid = 0;
            int i = 0;
            string grade;
            string suffix;
            List<string> storepapers = new List<string>();
            List<int> storegrades = new List<int>();

            //Pause in the code until a whole number is input
            do
            {
                Console.WriteLine("What is your student id?");
                input = Console.ReadLine();
                int.TryParse(input, out studentid);
                Console.Clear();
            } while (studentid < 1);

            //do loop to get the grade of someone with only to valid inputs to continue: 5 and 6
            do
            {
                Console.WriteLine("What level are you studying: level 5 or level 6?");
                grade = Console.ReadLine();
                int.TryParse(grade, out output);
                Console.Clear();
            } while (output < 5 || output > 6);

            //Changing the value of papers for it's frequent future use
            if (output == 5)
            {
                papers = 4;
            }
            else
            {
                papers = 3;
            }

            //Reiterate the information for the user so they can check if they've made a mistake
            Console.WriteLine("As a level {0} student you must have done {1} papers last semester.", output, papers);

            //Creates a suffix to correctly ask the user for the information
            do
            {
                if (i == 0)
                {
                    suffix = "st";
                }
                else if (i == 1)
                {
                    suffix = "nd";
                }
                else if (i == 2)
                {
                    suffix = "rd";
                }
                else
                {
                    suffix = "th";
                }

            //label to refer back to so that the same paper name isn't entered twice
            repeat:

                //Ask the user to enter the papers they did and the corresponding grade of that paper
                Console.WriteLine("Enter the papers you study?");
                Console.WriteLine("Enter the {0}{1} paper you did.", i + 1, suffix);
                input = Console.ReadLine();
                input.Replace(" ", string.Empty);
                input = input.ToUpper();

                //Checks whether the paper input has been entered before and if so, repeats the prompt asking for the paper
                if (storepapers.Contains(input))
                {
                    Console.WriteLine("You have already entered that value, please a different value.");
                    Thread.Sleep(1000);
                    Console.Clear();
                    goto repeat;
                }
                storepapers.Add(input);

                //Forces a number value for the grade to be input
                do
                {
                    output = -1;
                    Console.WriteLine("Out of 100, what grade did you get in {0}?", storepapers[i]);
                    input = Console.ReadLine();
                    try
                    {
                        output = int.Parse(input);
                    }
                    catch
                    { }
                } while (output < 0 || output > 100);
                storegrades.Add(output);
                Console.Clear();
                i++;
            } while (i < papers);

            //Prompts for a summary of information, which is stored in another method
            Console.WriteLine("Would you like to see a summary of you're information?");
            input = Console.ReadLine().ToLower();
            if (input == "yes" || input == "y")
            {
                Console.WriteLine();
                Task2method2(storepapers, storegrades, studentid, papers, i, grade);
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
            }
            else { }

            //Prompts for a grade point average, which is stored in another method
            Console.WriteLine();
            Console.WriteLine("Would you like to see your grade point average?");
            input = Console.ReadLine().ToLower();
            if (input == "yes" || input == "y")
            {
                Task2method3(storegrades, i, papers);
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
            }

            //Prompts for the appearance of which papers the student has gotten an A+ in, stored in another method
            Console.WriteLine();
            Console.WriteLine("Would you like to see in which papers you got an A+?");
            input = Console.ReadLine().ToLower();
            if (input == "yes" || input == "y")
            {
                Task2method4(storepapers, storegrades, i, papers);
            }
            Console.WriteLine();
            Console.WriteLine("Press Any Key to Continue");
            Console.ReadKey();
        }

        //Task2method2 deals with the conversion of the numerical grade values into letter values
        //Task2method2 holds the display for the grades (in letter form), student id information, and the studying level
        static void Task2method2(List<string> storepapers, List<int> storegrades, int studentid, int papers, int i, string grade)
        {
            for (i = 0; i < papers; i++)
            {
                Console.Write(storepapers[i]);
                Console.Write(" --- ");
                if (storegrades[i] > 89)
                {
                    Console.WriteLine("A+");
                }
                else if (storegrades[i] > 84 && storegrades[i] < 90)
                {
                    Console.WriteLine("A");
                }
                else if (storegrades[i] > 79 && storegrades[i] < 85)
                {
                    Console.WriteLine("A-");
                }
                else if (storegrades[i] > 74 && storegrades[i] < 80)
                {
                    Console.WriteLine("B+");
                }
                else if (storegrades[i] > 69 && storegrades[i] < 75)
                {
                    Console.WriteLine("B");
                }
                else if (storegrades[i] > 64 && storegrades[i] < 70)
                {
                    Console.WriteLine("B-");
                }
                else if (storegrades[i] > 59 && storegrades[i] < 65)
                {
                    Console.WriteLine("C+");
                }
                else if (storegrades[i] > 54 && storegrades[i] < 60)
                {
                    Console.WriteLine("C");
                }
                else if (storegrades[i] > 49 && storegrades[i] < 55)
                {
                    Console.WriteLine("C-");
                }
                else if (storegrades[i] > 39 && storegrades[i] < 50)
                {
                    Console.WriteLine("D");
                }
                else if (storegrades[i] < 40)
                {
                    Console.WriteLine("E");
                }
            }
            Console.WriteLine();
            Console.WriteLine("Your student id is {0}", studentid);
            Console.WriteLine();
            Console.WriteLine("You are studying level {0}", grade);
            Console.WriteLine();
        }

        //Task2method3 shows the value for the grade point average and is the method which displays that information
        static void Task2method3(List<int> storegrades, int i, int papers)
        {
            int gpa = 0;

            for (i = 0; i < papers; i++)
            {
                gpa = gpa + storegrades[i];
            }
            Console.WriteLine();
            if (papers == 4)
            {
                Console.WriteLine("Your grades were {0}, {1}, {2}, {3}; your grade point average is {4}", storegrades[0], storegrades[1], storegrades[2], storegrades[3], gpa / papers);
            }
            else if (papers == 3)
            {
                Console.WriteLine("Your grades were {0}, {1}, {2}; your grade point average is {3}", storegrades[0], storegrades[1], storegrades[2], gpa / papers);
            }

            //Friendly response to the user regarding whether they have succeeded or not
            if (gpa / papers > 50)
            {
                Console.WriteLine("Congratulations you passed!");
            }
            else
            {
                Console.WriteLine("Unfortunately, you did not pass the course.");
            }
        }

        //Task2method4 shows the quantity of A+ grades (grades that were numerical 90+) and which papers those grades were in
        static void Task2method4(List<string> storepapers, List<int> storegrades, int i, int papers)
        {
            for (i = 0; i < papers; i++)
            {
                if (storegrades[i] > 89)
                {
                    Console.WriteLine("In the {0} course you managed to score an A+", storepapers[i]);
                }
            }
        }

        //TASK 3 STARTS HERE

        static void Task3game(List<int> scores)
        {
            int score = 0;
            int i = 0;

            Console.Clear();

            do
            {
                Random guess = new Random();
                int random = guess.Next(1, 6);
                string input;
                int output = 0;

                do
                {
                    Console.WriteLine("Please input a number between 1 and 5");
                    input = Console.ReadLine();
                    int.TryParse(input, out output);
                    Console.Clear();
                } while (output < 1 || output > 5);

                if (output == random)
                {
                    Console.WriteLine("wow you did it, you guessed the number right");
                    score++;
                }
                else
                {
                    Console.WriteLine("Im sorry, but you guess it wrong");
                }
                Console.WriteLine($"Your score is {score}");
                i++;
            } while (i < 5);


            i = 0;
            Task3playagain(score, scores);
        }

        static void Task3playagain(int score, List<int> scores)
        {
            string answer;

            scores.Add(score);

            Console.WriteLine("Your previous scores were:");

            for (int i = 0; i < scores.Count; i++)
            {
                Console.WriteLine($"Game {i + 1}: {scores[i]}");
            }


            Console.WriteLine("do you want to play again??? Please type in yes or no");
            answer = Console.ReadLine();
            answer.ToLower();
            if (answer == "yes" || answer == "y")
            {
                 Console.Clear();
                Task3game(scores);
            }
        }


        //TASK 4 STARTS HERE

        //This method controls the population and re-population of the dictionary from empty.
        static void TaskFourFillDictionary()
        {
            var dictionary = new Dictionary<int, string>();

            Console.Clear();
            while (dictionary.Count < 5)
            {
                string food = TaskFourEnterFood(dictionary);
                Console.Clear();
                int rating = TaskFourEnterRating(dictionary, $"Please enter a unique rating (1 - 5) for '{food}', where 1 is your most favourite, and 5 is your least favourite.");
                dictionary.Add(rating, food);
            }
            TaskFourMenu(dictionary);
        }

        //This method works with TaskFourFillDictionary() to populate the dictionary with foods. It records and checks what foods have been entered.
        static string TaskFourEnterFood(Dictionary<int, string> dictionary)
        {

        Retry:

            string food;

            if (dictionary.Count > 0)
            {
                TaskFourDisplayOrder(dictionary, 0, "Your order so far:");
            }
            Console.WriteLine($"Please enter the name of a food for food number {dictionary.Count + 1}.");
            food = Console.ReadLine().ToUpper();
            if (food.Any(char.IsDigit) || dictionary.ContainsValue(food) || food == "")
            {
                TaskFourErrorMessage();
                goto Retry;
            }
            return food;
        }

        //This method works with TaskFourFillDictionary() to populate the dictionary with ratings, and with TaskFourChangeOrder() to re-order the dictionary. It records and checks what ratings have been entered.
        static int TaskFourEnterRating(Dictionary<int, string> dictionary, string message)
        {

        Retry:

            int rating;

            if (dictionary.Count > 0)
            {
                TaskFourDisplayOrder(dictionary, 0, "Your order so far:");
            }
            Console.WriteLine($"{message}");
            if (!(int.TryParse(Console.ReadLine(), out rating)) || rating > 5 || rating < 1 || dictionary.ContainsKey(rating))
            {
                TaskFourErrorMessage();
                goto Retry;
            }
            Console.Clear();
            return rating;
        }

        //This method contains the menu. It acts as a switch between methods.
        static void TaskFourMenu(Dictionary<int, string> dictionary)
        {
            int response;

            Console.WriteLine("+=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=+");
            Console.WriteLine("|What would you like to do now? Please enter the number (1-5) of your selection.|");
            Console.WriteLine("+=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=+");
            Console.WriteLine("1. View the Order");
            Console.WriteLine("2. Change the Order of All Foods");
            Console.WriteLine("3. Swap the Order of 2 Foods");
            Console.WriteLine("4. Change the Foods");
            Console.WriteLine("5. Exit to Main Menu");
            Console.WriteLine("+=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=+");
            if (int.TryParse(Console.ReadLine(), out response) && response < 6 && response > 0)
            {
                switch (response)
                {
                    case 1:
                        Console.Clear();
                        TaskFourDisplayOrder(dictionary, 0, "Here are your food ratings so far, in order of most favourite (1) to least favourite (5):");
                        TaskFourMenu(dictionary);
                        break;

                    case 2:
                        TaskFourChangeOrder(dictionary);
                        break;

                    case 3:
                        TaskFourSwapOrder(dictionary);
                        break;

                    case 4:
                        TaskFourFillDictionary();
                        break;

                    case 5:
                        break;
                }
            }
            else
            {
                TaskFourErrorMessage();
                TaskFourMenu(dictionary);
            }
        }

        //This method orders the most current version of the dictionary.
        static void TaskFourDisplayOrder(Dictionary<int, string> dictionary, int swap, string message)
        {
            Console.WriteLine(message);
            for (int i = 1; i < 6; i++)
            {
                if (dictionary.ContainsKey(i) && i != swap)
                {
                    Console.WriteLine($"{i}. {dictionary[i]}");
                }
            }
            Console.WriteLine();
        }

        //This method allows the user to completely re-order all of the foods in the dictionary.
        static void TaskFourChangeOrder(Dictionary<int, string> dictionary)
        {
            var food = new List<string>();

            for (int i = 1; i < 6; i++)
            {
                food.Add(dictionary[i]);
                dictionary.Remove(i);
            }
            for (int i = 0; i < 5; i++)
            {
                Console.Clear();
                dictionary.Add(TaskFourEnterRating(dictionary, $"Please enter a new rating for '{food[i]}'. The previous rating was {i + 1}."), food[i]);
            }
            TaskFourMenu(dictionary);
        }

        //This method allows the user to swap the order of 2 foods in the dictionary.
        static void TaskFourSwapOrder(Dictionary<int, string> dictionary)
        {
            int swapOne = 0;
            int swapTwo = 0;

            swapOne = TaskFourCheckSwap(swapOne, "first", dictionary);
            swapTwo = TaskFourCheckSwap(swapOne, "second", dictionary);
            string swapOneFood = dictionary[swapOne];
            dictionary.Remove(swapOne);
            string swapTwoFood = dictionary[swapTwo];
            dictionary.Remove(swapTwo);
            dictionary.Add(swapOne, swapTwoFood);
            dictionary.Add(swapTwo, swapOneFood);
            Console.Clear();
            Console.WriteLine("The ratings have now been swapped.");
            Console.WriteLine();
            TaskFourMenu(dictionary);
        }

        //This method checks the values being swapped in TaskFourSwapOrder().
        static int TaskFourCheckSwap(int swapOne, string message, Dictionary<int, string> dictionary)
        {
            Console.Clear();
            int value = 0;

        Retry:

            TaskFourDisplayOrder(dictionary, swapOne, $"Please enter the value (1 - 5) of the {message} food you'd like to swap.");
            if (!(int.TryParse(Console.ReadLine(), out value)) || value > 5 || value < 1 || value == swapOne)
            {
                TaskFourErrorMessage();
                goto Retry;
            }
            return value;
        }

        //This method holds the error message that is used across the program for various checks.
        static void TaskFourErrorMessage()
        {
            Console.Clear();
            Console.WriteLine("Sorry, that was not a valid response.");
            Console.WriteLine();
        }
    }
}
